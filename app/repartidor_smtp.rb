require 'mail'

class RepartidorSmtp
  def initialize(smtp_server, smtp_port, smtp_user, smtp_password)
    options = {
      address: smtp_server,
      port: smtp_port,
      user_name: smtp_user,
      password: smtp_password,
      authentication: :plain,
      enable_starttls_auto: true
    }
    if ENV['APP_MODE'] == 'test'
      activar_modo_test(File.join(__dir__, ['mails']))
    else
      Mail.defaults do
        delivery_method :smtp, options
      end
    end
  end

  def enviar_libro(libro_estampado, mail_destinatario)
    adjunto = File.read(libro_estampado)
    texto_mail = 'Hola, tu libro personalizado se encuentra adjunto. Que lo disfrutes.'
    mail = Mail.new do
      from 'Libro <npaez@untref.edu.ar>'
      to mail_destinatario
      subject 'Un libro para ti'
      body texto_mail
      add_file :filename => 'tu_libro.pdf', :content => adjunto, :mime_type => 'application/x-pdf'
    end
    mail.deliver!
  end

  def activar_modo_test(directorio_mails)
    Mail.defaults do
      delivery_method :file, :location => directorio_mails
    end
  end
end
