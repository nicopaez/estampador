require 'spec_helper'

describe EditorialDigital do

  it 'delega al estampador la generacion del estampado' do
    estampador = instance_double 'Estampador'
    expect(estampador).to receive(:texto_a_estampar=)
    expect(estampador).to receive(:estampar)

    repartidor = instance_double 'Repartidor'
    allow(repartidor).to receive(:enviar_libro)

    imprenta = EditorialDigital.new(estampador, repartidor)

    libro_a_estampar = 'no_importa_porque_no_se_usa'
    texto_estampa = 'no_importa'
    mail_destinatario = 'alguien@noimporta.com'
    imprenta.imprimir(libro_a_estampar, texto_estampa, mail_destinatario)

  end

  it 'envia por mail el libro estampado' do
    estampador = instance_double 'Estampador'
    allow(estampador).to receive(:texto_a_estampar=)
    allow(estampador).to receive(:estampar)

    repartidor = instance_double 'Repartidor'
    expect(repartidor).to receive(:enviar_libro)

    editorial = EditorialDigital.new(estampador, repartidor)

    libro_a_estampar = 'no_importa_porque_no_se_usa'
    texto_estampa = 'no_importa'
    mail_destinatario = 'alguien@noimporta.com'
    editorial.imprimir(libro_a_estampar, texto_estampa, mail_destinatario)
  end

  it 'lanza ErrorEditorial cuando el repartidor no está disponible' do
    estampador = instance_double 'Estampador'
    allow(estampador).to receive(:texto_a_estampar=)
    allow(estampador).to receive(:estampar)

    repartidor = instance_double 'Repartidor'
    allow(repartidor).to receive(:enviar_libro).and_raise(StandardError)

    editorial = EditorialDigital.new(estampador, repartidor)

    libro_a_estampar = 'no_importa_porque_no_se_usa'
    texto_estampa = 'no_importa'
    mail_destinatario = 'alguien@noimporta.com'

    expect{editorial.imprimir(libro_a_estampar, texto_estampa, mail_destinatario)}.to raise_error(ErrorEditorial)
  end

end
