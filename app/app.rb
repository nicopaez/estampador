require_relative './estampador_prawn'
require_relative './editorial_digital'
require_relative './repartidor_smtp'
require 'logger'

def crear_editorial
  repartidor_smtp = RepartidorSmtp.new(ENV['SMTP_SERVER'], ENV['SMTP_PORT'], ENV['SMTP_USERNAME'],
                                       ENV['SMTP_PASSWORD'])
  EditorialDigital.new(EstampadorPrawn.new, repartidor_smtp)
end

def main(libro_a_estampar, texto_estampa, mail_destinatario)
  logger = Logger.new('libros/estampador.log')
  logger.level = Logger::INFO
  editorial = crear_editorial
  libro_estampado = editorial.imprimir(libro_a_estampar, texto_estampa, mail_destinatario)
  logger.info("Libro generado para #{mail_destinatario}")
  puts "Estampador #{File.read(File.join(__dir__, 'VERSION.txt')).strip}"
  puts "Libro generado: #{libro_estampado}"
rescue StandardError => e
  logger.error("Error para #{mail_destinatario}")
  logger.error(e)
  puts 'Error, no se pudo completar la operación por favor verifique los datos ingresados'
end

main(ARGV[0], ARGV[1], ARGV[2]) if __FILE__ == $PROGRAM_NAME
