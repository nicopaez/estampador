require 'prawn'
class EstampadorPrawn
  attr_accessor :texto_a_estampar

  LIBRO_ESTAMPADO = 'libros/archivo_estampado.pdf'.freeze

  def self.crear_con_estampa(texto_a_estampar)
    estampador = EstampadorPrawn.new
    estampador.texto_a_estampar = texto_a_estampar
    estampador
  end

  def estampar(libro_original)
    libro_estampado = LIBRO_ESTAMPADO
    nombre_estampa = 'estampa'
    texto_estampa = @texto_a_estampar
    Prawn::Document.generate(libro_estampado, :template => libro_original) do
      create_stamp(nombre_estampa) do
        fill_color '993333'
        font('Courier', :size => 8) do
          draw_text texto_estampa, :at => [0, 0]
        end
        fill_color '000000'
      end
      stamp_at(nombre_estampa, [-10, -10])
    end
    libro_estampado
  end
end
