describe EstampadorPrawn do

  it 'al estampar crear un nuevo archivo' do
    texto_a_estampar = 'texto_a_estampar'
    estampador = EstampadorPrawn.crear_con_estampa(texto_a_estampar)
    libro_original = File.join(__dir__, 'data', 'libro1.pdf')
    libro_estampado = estampador.estampar(libro_original)
    expect(File.exist?(libro_estampado)).to be true
  end



  it 'se crea con un texto para estampar' do
    texto_a_estampar = 'texto_a_estampar'
    estampador = EstampadorPrawn.crear_con_estampa(texto_a_estampar)
    expect(estampador.texto_a_estampar).to eq texto_a_estampar
  end

  after do
    File.delete(EstampadorPrawn::LIBRO_ESTAMPADO) if File.exist?(EstampadorPrawn::LIBRO_ESTAMPADO)
  end
end
