require 'spec_helper'

describe RepartidorSmtp do

  it 'enviar mail al destinatario con el archivo indica' do
    smtp_server = 'noimporta.com'
    smtp_port = 587
    smtp_user = 'noimporta'
    smtp_password = 'noimporta'
    repartidor = RepartidorSmtp.new(smtp_server, smtp_port, smtp_user, smtp_password)
    repartidor.activar_modo_test(File.join(__dir__, ['mails']))
    mail_destinatario = 'alguien@noimporta.com'
    libro_estampado = File.join(__dir__, 'data', 'libro1.pdf')
    repartidor.enviar_libro(libro_estampado, mail_destinatario)
    archivo_del_mail = File.join(__dir__, 'mails', mail_destinatario)
    expect(File.exist?(archivo_del_mail)).to be true
  end

  after do
    archivo_del_mail = File.join(__dir__, 'mails', 'alguien@noimporta.com')
    File.delete(archivo_del_mail) if File.exist?(archivo_del_mail)
  end

end
