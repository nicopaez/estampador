# Estampador

![https://gitlab.com/nicopaez/estampador/-/pipelines](https://gitlab.com/nicopaez/estampador/badges/main/pipeline.svg) ![https://gitlab.com/nicopaez/estampador/-/releases](https://gitlab.com/nicopaez/estampador/-/badges/release.svg)

Este aplicativo está desarrollado en ruby (v 3.1.2).

# Desarrollo

    # instalación de dependencias
    bundle install

    # ejecución de tests
    bundle exec rake

    # ejecución de la aplicación, asegurarse de setear las variables SMTP_SERVER, SMTP_PORT, SMTP_USERNAME, SMTP_PASSWORD
    bundle exec ruby app/app.rb libros/libro1.pdf libro_para_alguien alguien@algunlugar.com
    
    

## Uso
El ejecutable final es empaquetado con Docker. Los distintos releases están publicados en la registry del proyecto en GitLab. La forma de ejecutar la aplicación es:

    docker run -v$PWD/libros:/app/libros --env-file=.env.prod registry.gitlab.com/nicopaez/estampador:2.0.0 libros/libro1.pdf 'libro_para_juan' mail_de_juan@algo.com
