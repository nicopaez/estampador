FROM ruby:3.1.2
LABEL AUTHOR=NicoPaez
COPY app app
COPY Gemfile* /app
WORKDIR /app
RUN mkdir libros
RUN bundle install --without development test
RUN useradd userapp
RUN chown -R userapp:userapp /app
USER userapp
ENTRYPOINT [ "ruby", "app.rb" ]
