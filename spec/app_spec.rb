require 'spec_helper'

describe 'EstampadorApp' do

  log_file = File.join(__dir__, '..', 'libros', 'estampador.log')

  it 'imprime la version de la aplicacion' do
    texto_estampar = 'este_archivo_es_de_nico'
    libro_a_estamapar = File.join(__dir__, 'data', 'libro1.pdf')
    mail_destinatario = 'unmail@noimporta.com'
    resultado = `APP_MODE=test ruby app/app.rb #{libro_a_estamapar} #{texto_estampar} #{mail_destinatario}`
    version = File.read(File.join(__dir__, '..' ,'app', 'VERSION.txt')).strip
    expect(resultado.strip).to include "Estampador #{version}"
  end

  it 'genera un nuevo archivo con la estampa' do
    texto_estampar = 'este_archivo_es_de_nico'
    libro_a_estamapar = File.join(__dir__, 'data', 'libro1.pdf')
    mail_destinatario = 'nicopaez@gmail.com'
    resultado = `APP_MODE=test && ruby app/app.rb #{libro_a_estamapar} #{texto_estampar} #{mail_destinatario}`
    expect(File.exist?(EstampadorPrawn::LIBRO_ESTAMPADO)).to be true
  end

  it 'imprime el nombre del archivo generado' do
    texto_estampar = 'este_archivo_es_de_nico'
    libro_a_estamapar = File.join(__dir__, 'data', 'libro1.pdf')
    mail_destinatario = 'unmail@noimporta.com'
    resultado = `APP_MODE=test ruby app/app.rb #{libro_a_estamapar} #{texto_estampar} #{mail_destinatario}`
    expect(resultado.strip).to include "Libro generado: #{EstampadorPrawn::LIBRO_ESTAMPADO}"
  end

  it 'envia el archivo generado por mail' do
    texto_estampar = 'este_archivo_es_de_nico'
    mail_destinatario = 'unmail@noimporta.com'
    libro_a_estamapar = File.join(__dir__, 'data', 'libro1.pdf')
    resultado = `APP_MODE=test ruby app/app.rb #{libro_a_estamapar} #{texto_estampar} #{mail_destinatario}`
    expect(resultado.strip).to include "Libro generado: #{EstampadorPrawn::LIBRO_ESTAMPADO}"
    archivo_resultante = File.join(__dir__, ['..','app','mails','unmail@noimporta.com'])
    expect(File.exist?(archivo_resultante)).to be true
    File.delete(archivo_resultante)
  end

  it 'cuando el archivo a estampar no existe falla amistosamente y no genera archivo estampado' do
    texto_estampar = 'este_archivo_es_de_nico'
    libro_a_estamapar = File.join(__dir__, 'data', 'no_existe.pdf')
    mail_destinatario = 'nicopaez@gmail.com'
    resultado = `APP_MODE=test ruby app/app.rb #{libro_a_estamapar} #{texto_estampar} #{mail_destinatario}`
    expect(resultado.strip).to eq 'Error, no se pudo completar la operación por favor verifique los datos ingresados'
    expect(File.exist?(EstampadorPrawn::LIBRO_ESTAMPADO)).to be false
    expect(File.exist?(log_file)).to be true
    expect(File.read(log_file).include?mail_destinatario).to be true
  end

  after do
    File.delete(EstampadorPrawn::LIBRO_ESTAMPADO) if File.exist?(EstampadorPrawn::LIBRO_ESTAMPADO)
    File.delete(log_file)
  end

end
