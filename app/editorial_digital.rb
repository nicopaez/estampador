require_relative './error_editorial'
class EditorialDigital
  def initialize(estampador, repartidor)
    @estampador = estampador
    @repartidor = repartidor
  end

  def imprimir(libro_a_estampar, texto_estampa, mail_destinatario)
    @estampador.texto_a_estampar = texto_estampa
    libro_estampado = @estampador.estampar(libro_a_estampar)
    @repartidor.enviar_libro(libro_estampado, mail_destinatario)
    libro_estampado
  rescue StandardError => e
    raise ErrorEditorial, e.message
  end
end
